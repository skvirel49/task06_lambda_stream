package com.bilas.methods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        LOGGER.info("add list size: ");
        int size = SCANNER.nextInt();
        count(size);
        countSum(size);
    }

    private static List generateRandomList(int size) {
        return Stream
                .generate(() -> (int) (Math.random() * size) + size)
                .limit(size)
                .collect(Collectors.toList());
    }

    private static void count(int size) {
        List list = generateRandomList(size);
        int[] ints = list.stream().mapToInt(value -> (int) value).toArray();
        IntSummaryStatistics statistics = Arrays.stream(ints).summaryStatistics();
        LOGGER.info("sum - " + statistics.getSum());
        LOGGER.info("min - " + statistics.getMin());
        LOGGER.info("max - " + statistics.getMax());
        LOGGER.info("average - " + statistics.getAverage());
        LOGGER.info("count - " + statistics.getCount());
    }

    public static void countSum(int size){
        List<Integer> list = generateRandomList(size);
        int[] ints = list.stream().mapToInt(value -> (int) value).toArray();

        Integer sum = Arrays.stream(ints).sum();
        LOGGER.info("sum - " + sum);
        Integer reduce = list.stream().reduce(0, Integer::sum);
        LOGGER.info("reduce - " + reduce);
    }
}
