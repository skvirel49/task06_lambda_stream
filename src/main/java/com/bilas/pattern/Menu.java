package com.bilas.pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);
    Map<Integer, String> menu;
    Map<Integer, Command> commandMap;

    public Menu() {
        menu = new LinkedHashMap<>();
        putMenu();
        commandMap = new LinkedHashMap<>();

    }

    void printMenu() {
        LOGGER.info("Menu:");
        for (Map.Entry<Integer, String> integerStringEntry : menu.entrySet()) {
            LOGGER.info(integerStringEntry.getKey() + " - " + integerStringEntry.getValue());
        }
    }

    private void putMenu(){
        menu.put(1, "Lambda function");
        menu.put(2, "Method reference");
        menu.put(3, "Anonymous class");
        menu.put(4, "Object of class");
        menu.put(0, "exit");
    }

    private void command(String s){
        commandMap.put(1, s1 -> LOGGER.info(s));
        commandMap.put(2, this::command);
        commandMap.put(3, new Command() {
            @Override
            public void command(String s) {
                LOGGER.info(s);
            }
        });
        commandMap.put(4, LOGGER::info);
    }


    private void execute(int number){
        commandMap.get(number);
    }
    void show(String message){
        Scanner scanner = new Scanner(System.in);
        printMenu();
        command(message);
        LOGGER.info("add key:");
        int key = scanner.nextInt();
        if (key == 0){
            System.exit(0);
        }
        execute(key);
    }
}
