package com.bilas.pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    public static void main(String[] args) {
        Menu menu = new Menu();

        while (true){
            LOGGER.info("add message:");
            String message = SCANNER.nextLine();
            menu.show(message);
        }
    }
}
