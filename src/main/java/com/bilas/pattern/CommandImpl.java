package com.bilas.pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandImpl implements Command {
    private static final Logger LOGGER = LogManager.getLogger(CommandImpl.class);

    @Override
    public void command(String s) {
        LOGGER.info("This is method reference command.");
    }
}
