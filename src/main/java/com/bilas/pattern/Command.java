package com.bilas.pattern;

@FunctionalInterface
interface Command {
    void command(String s);
}
