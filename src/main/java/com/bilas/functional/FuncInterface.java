package com.bilas.functional;

@FunctionalInterface
public interface FuncInterface {
    int function(int a, int b, int c);
}
