package com.bilas.functional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        FuncInterface funcInterface = (a, b, c) -> Math.max(a, Math.max(b, c));
        FuncInterface funcInterface1 = (a, b, c) -> (a + b + c) / 3;

        LOGGER.info("Add three numbers:");
        int x = SCANNER.nextInt();
        int y = SCANNER.nextInt();
        int z = SCANNER.nextInt();

        LOGGER.info("Max value: " + funcInterface.function(x, y, z));
        LOGGER.info("Average value: " + funcInterface1.function(x, y, z));
    }


}
